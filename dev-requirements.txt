-e .
chardet==2.3.0
cobertura-clover-transform==1.1.2
coverage==3.7.1
docutils==0.12
Jinja2==2.7.3
lxml==3.4.4
MarkupSafe==0.23
mock==1.0.1
nose==1.3.4
Pygments==2.0.2
setproctitle==1.1.8
six==1.8.0
Sphinx==1.2.3
sphinx-rtd-theme==0.1.6
