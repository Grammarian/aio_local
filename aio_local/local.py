import logging
import threading
import asyncio
import weakref

_logger = logging.getLogger(__name__)


def _get_task_or_thread(loop=None):
    """
    Get the current asyncio task or the executing thread if not executing within a task.
    """
    try:
        task = asyncio.Task.current_task(loop)
        if task:
            return task
    except AssertionError:
        # We are inside a non MainThread for which an event loop has not been set.
        # We want to execute the fall through case.
        pass

    # We are inside a Thread which has an event loop but not inside an asyncio task
    return threading.current_thread()


def _get_task_or_thread_id(task_or_thread):
    """
    Get an identifier for the given task or thread.
    """
    if isinstance(task_or_thread, threading.Thread):
        return task_or_thread.ident
    else:
        return id(task_or_thread)


class _LocalState:

    def __init__(self, loop):
        self._loop = loop
        self._per_task_state = {}

    def _remove_local_state(self, state_id):
        del self._per_task_state[state_id]

    def get_task_state(self):
        executor = _get_task_or_thread(self._loop)
        executor_id = _get_task_or_thread_id(executor)
        state = self._per_task_state.setdefault(executor_id, {})

        # Register something to dispose of the task or threads state when it is finalized
        if '_destructor' not in state:
            state['_destructor'] = weakref.ref(executor, lambda x: self._remove_local_state(executor_id))
        return state


class local:
    """
    This class implements task local storage, where each task has its own
    distinct values.

    The logical model is that same as threading.local.

    The implementation strategy is to give every instance a task state
    object, which stores a dictionary of values per task. When a
    property is accessed or written, the dictionary corresponding to
    the currently executing task or thread is read or written.
    
    NOTE TO FUTURE SELF: Because this class overrides __getattribute__,
    *every* dot reference goes through that method. That makes it very easy
    to get into an infinite loop. Similarly for setting attributes.

    When making changes to this class, be afraid -- be very afraid :)

    https://docs.python.org/3/reference/datamodel.html#object.__getattribute__
    """

    def __init__(self, loop=None):
        object.__setattr__(self, '_task_states', _LocalState(loop))

    def __getattribute__(self, name):
        task_states = object.__getattribute__(self, '_task_states')
        state = task_states.get_task_state()
        if name == '_state':  # pragma: no cover
            return state
        else:
            return state.get(name)

    def __setattr__(self, name, value):
        task_states = object.__getattribute__(self, '_task_states')
        state = task_states.get_task_state()
        state[name] = value


if __name__ == '__main__':  # pragma: no cover
    from concurrent.futures import ThreadPoolExecutor

    def _get_task_identity():
        executor = _get_task_or_thread()
        executor_id = _get_task_or_thread_id(executor)
        return executor_id

    def play_with_ids():
        @asyncio.coroutine
        def in_task():
            print("task id >>: {}".format(_get_task_identity()))
            yield from asyncio.sleep(1)
            print("task id <<: {}".format(_get_task_identity()))

        def in_thread():
            print("thread id >>: {}".format(_get_task_identity()))

            # Non-MainThreads aren't auto-assigned an event loop so do it explicitly
            asyncio.set_event_loop(asyncio.new_event_loop())
            inner_loop = asyncio.get_event_loop()

            tasks = [inner_loop.create_task(in_task()) for i in range(2)]
            inner_loop.run_until_complete(asyncio.gather(*tasks, loop=inner_loop))
            print("thread id <<: {}".format(_get_task_identity()))

        loop = asyncio.get_event_loop()
        tasks = [loop.create_task(in_task()) for i in range(2)]

        executor = ThreadPoolExecutor(8)
        threads = [loop.run_in_executor(executor, in_thread) for i in range(4)]
        print('waiting for tasks and threads...')
        loop.run_until_complete(asyncio.gather(*(tasks + threads), loop=loop))
        print('done')

    def play_with_local():
        the_local = local()
        the_local.some_value = 'outer state'
        print('outer >>: {}'.format(the_local._state))

        @asyncio.coroutine
        def in_inner_task(param):
            yield from asyncio.sleep(1)
            print('in_inner_task {}: {}'.format(param, the_local._state))

        @asyncio.coroutine
        def in_task(param):
            the_local.some_value = 'param: ' + param
            yield from in_inner_task(param)

        loop = asyncio.get_event_loop()
        tasks = [loop.create_task(in_task(str(i))) for i in range(4)]
        loop.run_until_complete(asyncio.gather(*tasks, loop=loop))
        print('outer <<: {}'.format(the_local._state))

        print('done')

    play_with_ids()
    play_with_local()
