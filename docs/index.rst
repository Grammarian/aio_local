.. HipChat Dace documentation master file, created by
   sphinx-quickstart on Mon Mar  2 20:35:50 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to aiolocals documentation!
===================================

aiolocals is a library for task local state management with aiohttp integration for tracking request ids.

Contents:

.. toctree::
    :maxdepth: 2

    usage
    api
    changelog
    Bitbucket <https://bitbucket.org/hipchat/aiolocals/>
    Bamboo <https://collaboration-bamboo.internal.atlassian.com/browse/HCP-AIOLOCALSUNIT/>
    JIRA <https://jira.atlassian.com/projects/HC/>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`










