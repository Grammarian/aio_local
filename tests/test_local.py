import gc
import asyncio
import asyncio.test_utils
from concurrent.futures import ThreadPoolExecutor

import asynctest
import time

import aio_local


class LocalTests(asynctest.TestCase):
    def setUp(self):
        self.loop = asyncio.get_event_loop()

    def test_local_within_tasks(self):
        mylocal = aio_local.local()

        @asyncio.coroutine
        def task1():
            self.assertIsNone(mylocal.value)

            yield from asyncio.sleep(1)
            self.assertIsNone(mylocal.value)

            mylocal.value = "Task 1"
            yield from asyncio.sleep(1)
            self.assertEqual('Task 1', mylocal.value)


        @asyncio.coroutine
        def task2():
            self.assertIsNone(mylocal.value)

            mylocal.value = "Task 2"
            self.assertEqual('Task 2', mylocal.value)

            yield from asyncio.sleep(1)
            self.assertEqual('Task 2', mylocal.value)

        self.loop.run_until_complete(asyncio.wait((task1(), task2()), loop=self.loop))

    def test_local_multiple(self):
        mylocal = aio_local.local()
        mylocal2 = aio_local.local()

        @asyncio.coroutine
        def task1():
            self.assertIsNone(mylocal.value)
            self.assertIsNone(mylocal2.value)

            yield from asyncio.sleep(1)
            self.assertIsNone(mylocal.value)
            self.assertIsNone(mylocal2.value)

            mylocal.value = "Task 1"
            mylocal2.value = "2 - Task 1"
            yield from asyncio.sleep(1)
            self.assertEqual('Task 1', mylocal.value)
            self.assertEqual('2 - Task 1', mylocal2.value)


        @asyncio.coroutine
        def task2():
            self.assertIsNone(mylocal.value)
            self.assertIsNone(mylocal2.value)

            mylocal.value = "Task 2"
            mylocal2.value = "2 - Task 2"
            self.assertEqual('Task 2', mylocal.value)
            self.assertEqual('2 - Task 2', mylocal2.value)

            yield from asyncio.sleep(1)
            self.assertEqual('Task 2', mylocal.value)
            self.assertEqual('2 - Task 2', mylocal2.value)

        self.loop.run_until_complete(asyncio.wait((task1(), task2()), loop=self.loop))

    def test_local_destroyed_after_use(self):
        mylocal = aio_local.local()

        @asyncio.coroutine
        def task1():
            mylocal.value = "Task 1"
            yield from asyncio.sleep(1)
            self.assertEqual('Task 1', mylocal.value)
            yield from asyncio.sleep(1)
            self.assertEqual('Task 1', mylocal.value)

        @asyncio.coroutine
        def task2():
            mylocal.value = "Task 2"
            yield from asyncio.sleep(1)
            self.assertEqual('Task 2', mylocal.value)
            yield from asyncio.sleep(1)
            self.assertEqual('Task 2', mylocal.value)

        @asyncio.coroutine
        def task3():
            mylocal.value = "Task 3"
            yield from asyncio.sleep(1)
            self.assertEqual('Task 3', mylocal.value)
            yield from asyncio.sleep(1)
            self.assertEqual('Task 3', mylocal.value)
            self.assertNotEqual(0, len(object.__getattribute__(mylocal, '_task_states')._per_task_state))

        self.loop.run_until_complete(asyncio.wait((task1(), task2(), task3()), loop=self.loop))

        gc.collect()

        self.assertEqual(0, len(object.__getattribute__(mylocal, '_task_states')._per_task_state))


    def test_local_in_threads(self):
        mylocal = aio_local.local()

        def thread1():
            mylocal.value = "Thread 1"
            time.sleep(1)
            self.assertEqual('Thread 1', mylocal.value)
            time.sleep(1)
            self.assertEqual('Thread 1', mylocal.value)

        def thread2():
            mylocal.value = "Thread 2"
            time.sleep(1)
            self.assertEqual('Thread 2', mylocal.value)
            time.sleep(1)
            self.assertEqual('Thread 2', mylocal.value)

        loop = asyncio.get_event_loop()

        executor = ThreadPoolExecutor(2)
        t1 = loop.run_in_executor(executor, thread1)
        t2 = loop.run_in_executor(executor, thread2)

        loop.run_until_complete(asyncio.wait((t1, t2), loop=self.loop))
        executor.shutdown(wait=True)
        executor = None  # remove last reference to threads

        gc.collect()

        self.assertEqual(0, len(object.__getattribute__(mylocal, '_task_states')._per_task_state))
