"""
aiolocals
---------
"""
from setuptools import setup

setup(
    name='aio_local',
    version='0.1.0.dev0',
    url='https://bitbucket.org/Grammarian/aio_local',
    license='APLv2',
    author='Phillip Piper',
    author_email='phillip.piper@gmail.com',
    description="Task local state management for Python 3's asyncio",
    long_description=__doc__,
    packages=['aio_local'],
    include_package_data=True,
    platforms='any',
    extras_require={
        "aiohttp": ["aiohttp>=0.15.0"],
    },
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
